<?php

    require_once('views/app_fns.php');
    $page = isset($_GET['page']) ? $_GET['page'] : 'home';

    html_fns_header();
    html_fns_menu();

    mainController($page);

    html_fns_footer();

?>