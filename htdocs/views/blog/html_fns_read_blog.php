<?php

function html_fns_read_blog()
{

    // get ID of the product to be read
    $id = isset($_GET['id']) ? $_GET['id'] : die('ERROR: missing ID.');
    
    // core configuration
    include_once "inner/config/core.php";
    // include models
    include_once "inner/config/database.php";
    include_once "inner/models/Entry.php";
    
    // get database connection
    $database = new Database();
    $db = $database->getConnection();
    
    // prepare objects
    $entry = new Entry();
    
    // set ID property of entry to be read
    $entry->id = $id;
    
    // read the details of entry to be read
    $entry->readOne($db);

    // set page headers
    $page_title = "Read One Entry";

    // HTML table for displaying a entries details
    echo "<table class='table'>";
    
        echo "<tr>";
            echo "<td>Title</td>";
            echo "<td>{$entry->title}</td>";
        echo "</tr>";

        echo "<tr>";
            echo "<td>Excerpt</td>";
            echo htmlspecialchars_decode("<td>{$entry->excerpt}</td>");
        echo "</tr>";
    
        echo "<tr>";
            echo "<td>Content</td>";
            echo htmlspecialchars_decode("<td><p>{$entry->content}</p></td>");
        echo "</tr>";
        echo "<tr>";
            echo "<td>Created</td>";
            echo htmlspecialchars_decode("<td><p>{$entry->created_at}</p></td>");
        echo "</tr>";
    
    echo "</table>";
    echo "<div class='d-flex flex-row-reverse'>";
        if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']==true && $_SESSION['access_level']=='admin'){
            echo "<div class='p-2'>";
                echo "<a delete-id='{$id}' class='btn btn-danger delete-object pull-right'>
                <span class='glyphicon glyphicon-remove'></span> Delete
                </a>";
            echo "</div>";
            echo "<div class='p-2'>";
                echo "<a href='index.php?page=blog&blog=update&id={$id}' class='btn btn-info pull-right'>
                <span class='glyphicon glyphicon-edit'></span> Edit
                </a>";
            echo "</div>";    
        }
            echo "<div class='p-2'>";
                echo "<a href='index.php' class='btn btn-primary pull-right'>
                        <span class='glyphicon glyphicon-list'></span> Read All Entries
                    </a>";
            echo "</div>";
        echo "</div>";
    echo "</div>";
}
?>

