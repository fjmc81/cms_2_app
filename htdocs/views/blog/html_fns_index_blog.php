<?php

//include_once('inner/controllers/index.php');

function html_fns_index_blog($action = '')
{

    // core configuration
    include_once "inner/config/core.php";
 
    // set page title
    $page_title="Index";
    
    // include login checker
    $require_login=true;
    //include_once "login_checker.php";
 
    echo "<div class='d-flex justify-content-end'>"; 
        echo "<div class='col-md-9'>";
        
            // to prevent undefined index notice
            $action = isset($_GET['action']) ? $_GET['action'] : "";
        
            // if login was successful
            if($action=='login_success'){
                echo "<div class='alert alert-info'>";
                    echo "<strong>Hi " . $_SESSION['username'] . ", welcome back!</strong>";
                echo "</div>";
            }
        
            // if user is already logged in, shown when user tries to access the login page
            else if($action=='already_logged_in'){
                echo "<div class='alert alert-info'>";
                    echo "<strong>You are already logged in.</strong>";
                echo "</div>";
            }

            // if login was successful
            if($action=='login_check'){
                echo "<div class='alert alert-info'>";
                    echo "<strong>Hi " . $_SESSION['username'] . ", welcome back!</strong>";
                echo "</div>";
            }
        
            // content once logged in        
            // Fetch the three most recent entries:
            try {

                // page given in URL parameter, default page is one
                $pages = isset($_GET['pages']) ? $_GET['pages'] : 1;
                
                // set number of records per page
                $records_per_page = 4;
                
                // calculate for the query LIMIT clause
                //$from_record_num = ((int)$records_per_page * (int)$page) - (int)$records_per_page;
                $from_record_num = ((int)$records_per_page * (int)$pages) - (int)$records_per_page;

                // include models
                include_once "inner/config/database.php";
                include_once "inner/models/Entry.php";
                
                // instantiate database and objects
                $database = new Database();
                $db = $database->getConnection();
                
                $entry = new Entry($db);  

                // query products
                $stmt = $entry->readAll($db, $from_record_num, $records_per_page);
                $stmt2 = $entry->readAllNoLimits($db);
                $num = $stmt2->rowCount();
                $total_rows = $num;

                if($num>0){

                        // Records will be fetched in the view:
                        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                            extract($row);
                            echo "<article>
                            <h1>{$title}</h1>";
                            echo htmlspecialchars_decode("<p>{$excerpt}</p>");
                            echo htmlspecialchars_decode("<p>Created at: {$created_at}</p>");
                            //echo htmlspecialchars_decode("<p>{$updated_at}</p>");
                            echo "</article>
                            <div class='d-flex justify-content-around'>
                                <a href='index.php?page=blog&blog=read&id={$id}' class='btn btn-primary right-margin'>
                                    <span class='glyphicon glyphicon-list'></span> Read entry
                                </a>
                            </div>
                            ";
                        }
                        include_once('inner/bundles/blogBundle/html_fns_pagination.php');
                        html_fns_pagination($pages, $total_rows, $records_per_page);

                } else { // Problem!
                    throw new Exception('No content is available to be viewed at this time.');
                }
                    
            } catch (Exception $e) { // Catch generic Exceptions.
                throw new Exception('No content is available to be viewed at this time.');
            }

        echo "</div>";
    echo "</div>";
    }
?>