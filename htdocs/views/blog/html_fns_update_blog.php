<?php

function html_fns_update_blog()
{
    //require_one("vendor/ckeditor/ckeditor/ckeditor.php");
    // include models
    include_once "inner/models/Entry.php";
    
    // set page title
    $page_title="Update Entry";
    
    // include login checker
    $require_login=false;
    include_once "inner/bundles/loginBundle/login_checker.php";
    
    // get ID of the entry to be edited
    $id = isset($_GET['id']) ? $_GET['id'] : die('ERROR: missing ID.');
    
    // get database connection
    $database = new Database();
    $db = $database->getConnection();
    
    // prepare objects
    $entry = new Entry($db);
    
    // set ID property of entry to be edited
    $entry->id = $id;
    
    // read the details of entry to be edited
    $entry->readOne($db);

    // if the form was submitted
    if($_POST){
    
        // set entry property values
        $entry->title = $_POST['title'];
        $entry->excerpt = $_POST['excerpt'];
        $entry->content = $_POST['content'];
        $entry->updated_at = $_POST['updated_at'];
        
        // update the entry
        if($entry->update($db)){
            echo "<div class='alert alert-success alert-dismissable'>";
                echo "Entry was updated.";
            echo "</div>";
        }
    
        // if unable to update the entry, tell the user
        else{
            echo "<div class='alert alert-danger alert-dismissable'>";
                echo "Unable to update entry.";
            echo "</div>";
        }
    }

    ?>
    
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]. "?page=blog&blog=update&id={$id}");?>" method="post">
        <table class='table'>
    
            <tr>
                <td>Title</td>
                <td><input type='text' name='title' value='<?php echo $entry->title; ?>' class='form-control' required /></td>
            </tr>

            <tr>
                <td>Excerpt</td>
                <td><textarea name='excerpt' class='ckeditor' required><?php echo htmlspecialchars_decode($entry->excerpt, ENT_HTML5 ); ?></textarea></td>
            </tr>
    
            <tr>
                <td>Content</td>
                <td><textarea name='content' class='ckeditor' required><?php echo htmlspecialchars_decode($entry->content, ENT_HTML5 ); ?></textarea></td>
            </tr>
            <tr>
            <td>Updated</td>
                <td><input type='date' name='updated_at' value='GETDATE()' class='form-control' required /></td>
            </tr>
    
            <tr>
                <td></td>
                <td>
                    <button type="submit" class="btn btn-primary">Update</button>
                </td>
            </tr>
    
        </table>
    </form>

<?php
}
?>