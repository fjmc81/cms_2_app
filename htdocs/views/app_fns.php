<?php

    require_once __DIR__.'\..\vendor\autoload.php';

    require_once('inner/config/core.php');
    require_once('inner/bundles/loginBundle/login_checker.php');

    require_once('views/base/html_fns_menu.php');
    require_once('views/base/html_fns_header.php');

    require_once('inner/controllers/mainController.php');

    require_once('views/base/html_fns_footer.php');

?>