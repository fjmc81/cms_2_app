<?php

function html_fns_index_home()
{
?>
    <div class='container'>
        <?php
            include_once('views/landing_page/_jumbotron-home.html');
        ?>
    </div>

    <main role="main" class="container">
      <div class="row">
        <div class="col-md-8 blog-main">
          <h3 class="pb-3 mb-4 font-italic border-bottom">
            Welcome Homepage
          </h3>
        
          <?php
            include_once('views/blog/html_fns_index_blog.php');
            html_fns_index_blog();
          ?>

        </div><!-- /.blog-main -->

        
      </div><!-- /.row -->

    </main><!-- /.container -->


<?php
}  
?>