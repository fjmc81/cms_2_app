<?php
require_once('inner/config/core.php');

function html_fns_menu()
{
?>
<div class="nav-scroller py-1 mb-2">
      
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="index.php">FC</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            
            <li class="nav-item">
              <a class="nav-link" href="index.php?page=blog">Blog</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="index.php?page=about">About</a>
            </li>
            <?php
            if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']==true && $_SESSION['access_level']=='admin'){
            ?>
                <li class="nav-item">
                    <a class="nav-link" href="index.php?page=blog&blog=create" tabindex="-1">Create</a>
                </li>
            <?php
            }
            ?>
            <li class="nav-item">
              <?php
                // check if users / customer was logged in
                // if user was logged in, show "Edit Profile", "Orders" and "Logout" options
                if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']==true && $_SESSION['access_level']=='admin'){
                    ?>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                <?php echo $_SESSION['username']; ?>
                                <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="index.php?page=user&user_check=logout"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
                            </div>
                        </li>
                    </ul>
                    <?php
                    } 
                    // if user was not logged in, show the "login" and "register" options
                    else{
                    ?>
                      <a class="nav-link" href="index.php?page=user&user_check=login">
                      <i class="fa fa-sign-in" aria-hidden="true"></i> Log In
                      </a>
                    <?php
                    } 
                ?>
            </li>
          </ul>
          <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
          </form>
        </div>
      </nav>

    </div>
<?php
}

?>