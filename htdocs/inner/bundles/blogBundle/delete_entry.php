<?php
// check if value was posted
if($_POST){
    
    // include database and object file
    include_once __DIR__."\..\..\..\inner\models\Entry.php";
  
    // get database connection
    $database = new Database();
    $db = $database->getConnection();
  
    // prepare entry object
    $entry = new Entry($db);
      
    // set entry id to be deleted
    $entry->id = $_POST['object_id'];
    var_dump($_POST['object_id']);
      
    // delete the entry
    if($entry->delete($db)){
        echo "Object was deleted.";
    }
      
    // if unable to delete the entry
    else{
        echo "Unable to delete object.";
    }
}
?>