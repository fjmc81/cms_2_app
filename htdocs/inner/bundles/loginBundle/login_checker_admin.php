<?php
// login checker for 'admin' access level
 
// if the session value is empty, he is not yet logged in, redirect him to login page
if(empty($_SESSION['logged_in'])){
    header("Location: index.php?page=login&action=please_login");
}

// if access level was not 'Admin' but is 'User", redirect him to home page
else if($_SESSION['access_level']!="admin"){
    header("Location: index.php?page=admin&action=admin-users");
}
else{
    //no problem, stay there.
}
?>