<?php

function mainController($page)
{

    //here controllers
    switch($page) {
        default: //Default
            require_once('views/landing_page/html_fns_index_home.php');
            html_fns_index_home();
            break;
        //case '': //When it is null
        //    require_once('templates/landing_page/html_fns_index.php');
        //    html_fns_index();
        case 'blog':
            require_once('inner/controllers/blogController.php');
            blogController($page);
            break;
        case 'user':
            require_once('inner/controllers/loginController.php');
            loginController($page);
            break;

    }
    
}
?>