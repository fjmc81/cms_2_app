<?php

function blogController($action)
{

    include_once('inner/models/Entry.php');

    //here actions
    //index.php?page=blog&blog=read&id=1
    $action = isset($_GET['blog']) ? $_GET['blog'] : 'list';
    switch($action) {
        case 'list':
            require_once('inner/bundles/blogBundle/html_fns_paging.php');
            require_once('views/blog/html_fns_index_blog.php');
            html_fns_index_blog();
            break;
        case 'read':
            require_once('views/blog/html_fns_read_blog.php');
            html_fns_read_blog();
            break;
        case 'update':
            require_once('views/blog/html_fns_update_blog.php');
            html_fns_update_blog();
            break;
        case 'create':
            require_once('inner/bundles/blogBundle/create_entry.php');
            require_once('views/blog/html_fns_create_blog.php');
            html_fns_create_blog();
            break;
        case 'delete':
            require_once('inner/bundles/blogBundle/delete_entry.php');
            break;
    }
    
}
?>