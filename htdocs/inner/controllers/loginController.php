<?php

function loginController($action)
{

    include_once('inner/models/User.php');

    //here actions
    //index.php?page=user&user_check=login
    $action = isset($_GET['user_check']) ? $_GET['user_check'] : 'login';
    switch($action) {
        case 'login':
            require_once('inner/bundles/loginBundle/login.php');
            require_once('views/login/html_fns_login.php');
            html_fns_login();
            break;
        case 'logout':
            require_once('inner/bundles/loginBundle/logout.php');
            require_once('views/login/html_fns_logout.php');
            html_fns_logout();
            break;

    }
    
}
?>